using Plots

function test_fun()
    a    = rand(1:5,10)
    sum(a)
    x = collect(1:2:4)
end

function test_b()
    a    = rand(1:5,10)
    sum(a)
    x = collect(1:2:4)
end


function main()
    test_b()
    test_fun()
    return 1
end

Juno.@enter main()
